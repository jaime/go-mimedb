package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"time"
)

var versionRegexp = regexp.MustCompile(`^v\d+\.\d+\.\d+`)

const (
	timeout = 30 * time.Second

	genFile     = "generated_mime_types.go"
	versionFile = "VERSION"
)

type MimeType struct {
	Source       string   `json:"source"`
	Extensions   []string `json:"extensions"`
	Compressible bool     `json:"compressible"`
	Charset      string   `json:"charset"`
}

type Data map[string]MimeType

func main() {
	os.Remove(genFile)

	out, err := os.OpenFile(genFile, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		panic(err)
	}

	defer out.Close()

	f, err := os.Open(versionFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	v, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}

	client := http.Client{
		Timeout: timeout,
	}

	rsp, err := client.Get(fmt.Sprintf("https://cdn.rawgit.com/jshttp/mime-db/%s/db.json", versionRegexp.FindString(string(v))))
	if err != nil {
		panic(err)
	}

	if rsp.StatusCode != http.StatusOK {
		panic(rsp)
	}

	var result Data

	if err := json.NewDecoder(rsp.Body).Decode(&result); err != nil {
		panic(err)
	}

	fmt.Fprintln(out, "package mimedb")
	fmt.Fprintln(out, "")
	fmt.Fprintln(out, "var (")
	fmt.Fprintln(out, "	mimeTypeToExts = map[string][]string{")

	for mimeType, entry := range result {
		if len(entry.Extensions) > 0 {
			fmt.Fprintf(out, "		%q: %#v,\n", mimeType, entry.Extensions)
		}
	}

	fmt.Fprintln(out, "	}")
	fmt.Fprintln(out, ")")

	if err := out.Close(); err != nil {
		panic(err)
	}

	// run `go fmt` on the output
	if err := exec.Command("go", "fmt").Run(); err != nil {
		panic(err)
	}
}
